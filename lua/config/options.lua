-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
--
local opt = vim.opt

opt.title = true
opt.background = "dark"
opt.guifont = "JetBrains Mono:h12"
opt.wrap = true

vim.g.tex_flavor = "context"

vim.filetype.add({
  extension = {
    asm = "nasm",
    mkxl = "context",
    mkiv = "context",
    ly = "lilypond",
    lfg = "lua",
    lmt = "lua",
    nu = "nu",
  },
  filename = {
    ["/etc/nftables.conf"] = "nftables",
  },
})
