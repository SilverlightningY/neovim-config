return {
  "gpanders/nvim-parinfer",
  ft = { "clojure", "scheme", "lisp", "racket", "hy", "fennel", "janet", "carp", "wast", "yuck", "query" },
}
