return {
  {
    "neovim/nvim-lspconfig",
    opts = {
      ---@type lspconfig.options
      ---@diagnostic disable-next-line: missing-fields
      servers = {
        ---@diagnostic disable-next-line: missing-fields
        tsserver = {},
        eslint = {
          ---@diagnostic disable-next-line: missing-fields
          settings = {
            format = {
              enable = true,
            },
            experimental = {
              useFlatConfig = true,
            },
            autoFixOnSave = true,
          },
        },
        ---@diagnostic disable-next-line: missing-fields
        clangd = {
          mason = false,
        },
        ---@diagnostic disable-next-line: missing-fields
        rust_analyzer = {
          mason = false,
        },
        ---@diagnostic disable-next-line: missing-fields
        jdtls = {},
        ---@diagnostic disable-next-line: missing-fields
        denols = {
          mason = false,
        },
        ---@diagnostic disable-next-line: missing-fields
        svelte = {},
        ---@diagnostic disable-next-line: missing-fields
        lemminx = {},
        qmlls = {
          mason = false,
        },
      },
      setup = {
        eslint = function(_, opts)
          require("lazyvim.util").lsp.on_attach(function(client)
            if client.name == "eslint" then
              client.server_capabilities.documentFormattingProvider = true
            elseif client.name == "tsserver" then
              client.server_capabilities.documentFormattingProvider = false
            end
          end)
          opts.autostart = false
        end,
        clangd = function(_, opts)
          opts.autostart = false
        end,
        astro = function(_, opts)
          opts.autostart = false
        end,
        rust_analyzer = function(_, opts)
          opts.autostart = false
        end,
        jdtls = function(_, opts)
          opts.autostart = false
        end,
        denols = function(_, opts)
          opts.autostart = false
        end,
        svelte = function(_, opts)
          opts.autostart = false
        end,
        lemminx = function(_, opts)
          opts.autostart = false
        end,
        dprint = function(_, opts)
          opts.autostart = false
        end,
        lua_ls = function(_, opts)
          opts.autostart = false
        end,
        tsserver = function(_, opts)
          opts.autostart = false
        end,
        tailwindcss = function(_, opts)
          opts.autostart = false
        end,
        qmlls = function(_, opts)
          if vim.fn.executable("qmlls6") == 1 then
            opts.cmd = { "qmlls6" }
          end
          opts.autostart = false
        end,
        ["*"] = function(_, opts)
          opts.autostart = false
        end,
      },
    },
  },
}
