return {
  "nvim-lualine/lualine.nvim",
  event = "VeryLazy",
  opts = function(_, opts)
    local icons = require("lazyvim.config").icons
    table.insert(opts.sections.lualine_y, "filetype")
    opts.sections.lualine_z = { "encoding" }
    opts.sections.lualine_c = {
      {
        "diagnostics",
        symbols = {
          error = icons.diagnostics.Error,
          warn = icons.diagnostics.Warn,
          info = icons.diagnostics.Info,
          hint = icons.diagnostics.Hint,
        },
      },
      { "filename", path = 1 },
      -- stylua: ignore
      {
        function() return require("nvim-navic").get_location() end,
        cond = function() return package.loaded["nvim-navic"] and require("nvim-navic").is_available() end,
      },
    }
    opts.tabline = {
      lualine_a = {
        {
          "windows",
          filetype_names = {
            NvimTree = "NvimTree",
            TelescopePrompt = "Telescope",
          },
        },
      },
      lualine_z = {
        {
          "tabs",
        },
      },
    }
  end,
}
